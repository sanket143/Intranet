import React from "react";
import {
  ActivityIndicator,
  Alert,
  Text,
  FlatList,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { Icon } from "native-base";


class IntranetDir extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      currentLocation: [],
      isLoading: true
    }
  }

  componentDidMount(){
    let url = "http://intranet.daiict.ac.in/~daiict_nt01/";
    this.extractSource(url);
  }

  extractSource(url){
    return fetch(url)
      .then((response) => response.text())
      .then((html) => {
        console.log(html);
        const regex = /<tr>(.*?)alt="\[(.*?)\]"(.*?)href="(.*?)">(.*?)<\/a>(.*?)"right">(.*?)<\/td>(.*?)<\/tr>/gi

        let result, source = [];
        while((result = regex.exec(html))){
          source.push(result);
        }

        source = source.map(function(snippet){
          let obj = {
            type: snippet[2].trim(),
            href: snippet[4].trim(),
            name: snippet[5].trim(),
            timestamp: snippet[7].trim()
          }

          return obj;
        })
        if(!this.state.currentLocation.length){
          source.splice(0, 1);
        } else {
          source[0].href = "/";
        }
        this.setState({
          isLoading: false,
          dataSource: source,
        }, function(){
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  changeLocation(locn){
    this.setState({
      isLoading: true
    })
    let head = "http://intranet.daiict.ac.in/~daiict_nt01/";

    if(locn == "/"){
      this.state.currentLocation.pop();
    } else {
      this.state.currentLocation.push(locn);
    }
    url_locn = this.state.currentLocation.join("/");
    let url = head + url_locn;

    this.extractSource(url);
  }

  static navigationOptions = {
    title: "Intranet",
    headerRight: <Icon
      style={{
        padding: 10
      }}
      type="Ionicons"
      name="at" />
  }

  render(){

    if(this.state.isLoading){
      return(
        <View style={{
          flex: 1,
          padding: 20,
          justifyContent: "center",
          backgroundColor: "white",
          alignItems: "center"}}
        >
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{
        flex: 1,
        padding: 10,
        backgroundColor: "white"
      }}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => 
            <TouchableOpacity
              onPress = {() => {
                this.changeLocation(item.href);
              }}
            >
              <View style={{
                justifyContent: "center",
                padding: 5,
              }}>
                <Icon
                  type="Ionicons"
                  name="folder-open"
                />
                <Text style={ styles.dir }>
                  {item.name}
                </Text>
              </View>
            </TouchableOpacity>
          }
        />
      </View>
    );
  }
}

export default IntranetDir;
const styles = StyleSheet.create({
  dir: {
    fontSize: 15,
    borderStyle: "solid",
    borderBottomWidth: 1,
    borderColor: "#eee",
    justifyContent: "center"
  }
});
