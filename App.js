import React from 'react';
import { ScrollView, ActivityIndicator, Text, FlatList, Image, View, Alert, StyleSheet, Button, TextInput } from 'react-native';

import { StackNavigator } from "react-navigation";
import IntranetDir from "./Components/IntranetDir";
export default class App extends React.Component {

  render(){

    return(
      <AppStackNavigator />
    );
  }
}

const AppStackNavigator = StackNavigator({
  Main: {
    screen: IntranetDir
  }
});
